from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='ros2_zenoh_bridge',
            executable='ros2_zenoh_bridge',
            name='default_ros2_zenoh_bridge_node',
            output='screen',
            emulate_tty=True,
            parameters=[
                {'topics': 
"""{
    "ros_to_zenoh": [
        { "ros": "/topic", "zenoh": "topic", "congestion": "BLOCK"}    
    ],
    "zenoh_to_ros": [
        { "zenoh": "topic", "ros": "/topic_back", "reliability": "RELIABLE"}        
    ]
}"""}
            ]
        )
    ])