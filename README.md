# ros2_zenoh_bridge

This ROS2 package creates a ROS2 <-> Zenoh bridge. It relies on ROS2 serialization.

## Node parameters

### zenoh_config

This parameter is the json5 [zenoh configuration](https://zenoh.io/docs/manual/configuration/)

### topics

This parameter has two attributes:
* The *ros_to_zenoh* attribute is the list of ROS2 topics to replicate in Zenoh. Each entry consists of three attributes:
    1. the *ros* attribute, which is the name of the ROS topic,
    2. the *zenoh* attribute, which is the corresponding zenoh key
    3. and the *congestion* attribute is used to specify the [congestion control](https://zenoh-python.readthedocs.io/en/0.11.0-rc.2/#congestioncontrol), it can be either "BLOCK" or "DROP".

* The *zenoh_to_ros* attribute is the list of Zenoh topics to replicate to ROS2. Each entry consists of three attributes:
    1. the *zenoh* attribute, which is the corresponding zenoh key,
    2. the *ros* attribute, which is the name of the ROS topic
    3. and the *reliability* attribute is used to specify the [reliability](https://zenoh-python.readthedocs.io/en/0.11.0-rc.2/index.html#zenoh.Reliability), it can be either "BEST_EFFORT" or "RELIABLE".

```json
{
    "ros_to_zenoh": [
        { "ros": "/topic", "zenoh": "topic", "congestion": "BLOCK"}    
    ],
    "zenoh_to_ros": [
        { "zenoh": "topic", "ros": "/topic_back", "reliability": "RELIABLE"}        
    ]
}
```

## How to use

1. build and install this package
2. run the other nodes. Since this bridge derives the message type from the current publishers and subscribers, it is mandatory to start them first!
3. run zenoh (*zenohd*)
4. start the bridge
