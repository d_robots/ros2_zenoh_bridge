import rclpy
from rclpy.node import Node
from rosidl_runtime_py.utilities import get_message
from rclpy.serialization import serialize_message, deserialize_message

import zenoh
import json

def get_msg_type(node, topic):
    # Utilisation de la fonctionnalité d'introspection pour obtenir le type de message
    topic_names_and_types = node.get_topic_names_and_types()
    for name, types in topic_names_and_types:
        if name == topic:
            for type_str in types:
                try:
                    return get_message(type_str)
                except (AttributeError, ModuleNotFoundError):
                    node.get_logger().warn(f"Could not import message type {type_str}")
    node.get_logger().error(f"Could not find message type for topic {topic}")
    return None

class RosToZenoh():
    def __init__(self, node, session, ros_topic: str, zenoh_topic: str, congestion: str) -> None:
        self.__node = node
        self.__session = session
        self.__ros_topic = ros_topic
        self.__zenoh_topic = zenoh_topic
        # ROS
        self.__msg_type = get_msg_type(self.node, ros_topic)
        self.__subscription = self.node.create_subscription(
            self.msg_type,
            self.ros_topic,
            self.callback,
            10
        )
        # Zenoh
        self.__congestion = zenoh.CongestionControl.BLOCK()
        if congestion == 'DROP': 
            self.__congestion = zenoh.CongestionControl.DROP()
        self.__publisher = self.session.declare_publisher(self.zenoh_topic, congestion_control=self.congestion)

    @property
    def node(self): return self.__node
    @property
    def session(self): return self.__session
    @property
    def ros_topic(self): return self.__ros_topic
    @property
    def zenoh_topic(self): return self.__zenoh_topic
    @property
    def msg_type(self): return self.__msg_type
    @property
    def subscription(self): return self.__subscription
    @property
    def publisher(self): return self.__publisher
    @property
    def congestion(self): return self.__congestion

    def callback(self, msg):
        self.node.get_logger().info(f"ros {self.ros_topic} -> zenoh {self.zenoh_topic}: {msg}")
        serialized_data = serialize_message(msg)
        self.publisher.put(serialized_data)

class ZenohToRos():
    def __init__(self, node, session, zenoh_topic: str, ros_topic: str, reliability: str) -> None:
        self.__node = node
        self.__session = session
        self.__zenoh_topic = zenoh_topic
        self.__ros_topic = ros_topic
        # Zenoh
        self.__reliability = zenoh.enums.Reliability.RELIABLE()
        if reliability == 'BEST_EFFORT':
            self.__reliability = zenoh.enums.Reliability.BEST_EFFORT()
        self.__subscriber = self.session.declare_subscriber(
            keyexpr=self.zenoh_topic,
            handler=self.listener,
            reliability=self.reliability
        )
        # ROS
        self.__msg_type = get_msg_type(self.node, ros_topic)
        self.__publisher = self.node.create_publisher(
            self.msg_type,
            self.ros_topic,
            10
        )

    @property
    def node(self): return self.__node
    @property
    def session(self): return self.__session
    @property
    def ros_topic(self): return self.__ros_topic
    @property
    def zenoh_topic(self): return self.__zenoh_topic
    @property
    def msg_type(self): return self.__msg_type
    @property
    def subscriber(self): return self.__subscriber
    @property
    def publisher(self): return self.__publisher
    @property
    def reliability(self): return self.__reliability

    def listener(self, sample: zenoh.Sample):
        msg = sample.payload
        deserialized_data = deserialize_message(msg, self.msg_type)
        self.node.get_logger().info(f"zenoh {self.zenoh_topic} -> ros {self.ros_topic}: {msg}")
        self.publisher.publish(deserialized_data)


class Ros2ZenohBridge(Node):
    def __init__(self):
        super().__init__('ros2_zenoh_bridge')

        # ------------------------- Parameters -------------------------
        # Zenoh Config
        self.declare_parameter('zenoh_config', 
"""{
    "mode": "client",
    "connect": { "endpoints": [ "tcp/127.0.0.1:7447" ]},
    "scouting": { 
        "multicast": { "enabled": false, "listen": false }, 
        "gossip"   : { "enabled": false }
    },
}""")
        # ROS2 & Zenoh topics
        self.declare_parameter('topics', 
"""{
    "ros_to_zenoh": [],
    "zenoh_to_ros": []
}""")
        
        # ------------------------- Topics list -------------------------

        param = self.get_parameter('topics').get_parameter_value().string_value
        self.__topics = json.loads(param)

        # ------------------------- Zenoh -------------------------

        param = self.get_parameter('zenoh_config').get_parameter_value().string_value
        self.__zenoh_config = zenoh.Config.from_json5(param)
        self.__zenoh_session = zenoh.open(self.__zenoh_config)

        # ------------------------- Ros -> Zenoh -------------------------
        self.__ros_to_zenoh = {}
        for x in self.__topics['ros_to_zenoh']:
            ros_topic = x['ros']
            zenoh_topic = x['zenoh']
            congestion = x['congestion']
            binding = RosToZenoh(self, self.__zenoh_session, ros_topic, zenoh_topic, congestion)
            self.__ros_to_zenoh[ros_topic] = binding

        # ------------------------- Zenoh -> Ros -------------------------
        self.__zenoh_to_ros = {}
        for x in self.__topics['zenoh_to_ros']:
            ros_topic = x['ros']
            zenoh_topic = x['zenoh']
            reliability = x['reliability']
            binding = ZenohToRos(self, self.__zenoh_session, zenoh_topic, ros_topic, reliability)
            self.__zenoh_to_ros[zenoh_topic] = binding


def main(args=None):
    rclpy.init(args=args)

    bridge = Ros2ZenohBridge()

    rclpy.spin(bridge)

    bridge.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()