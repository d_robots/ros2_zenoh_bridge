from setuptools import find_packages, setup
import os
from glob import glob

package_name = 'ros2_zenoh_bridge'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*launch.[pxy][yma]*'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ddoose',
    maintainer_email='david.doose@gmail.com',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'ros2_zenoh_bridge = ros2_zenoh_bridge.ros2_zenoh_bridge_node:main'
        ],
    },
)
